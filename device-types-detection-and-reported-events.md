This file describes what axis and buttons are reported by each device type (joystick, gamepad, steering wheel), and how to detect those device types.

Input event devices that are marked with `INPUT_PROP_ACCELEROMETER` are filtered out by libmanette since it only monitors `ID_INPUT_JOYSTICK` devices from udev, so we can pretty much be sure there won't be any accelerometer detected as joystick (unless the driver didn't include it but in this case it should be fixed, e.g. Wiimote's accelerometer isn't currently reported as such).

# Gamepads

The expected gamepad mapping is pretty much extensively described in the [kernel documentation](https://www.kernel.org/doc/html/latest/input/gamepad.html). The only uncovered buttons in the documentation are `BTN_C` and `BTN_Z`, which are extra buttons present on some gamepads, that are next to the ABXY buttons (see [this image](https://upload.wikimedia.org/wikipedia/commons/thumb/6/66/Sega-Genesis-6But-Cont.jpg/1024px-Sega-Genesis-6But-Cont.jpg) for example). To detect a gamepad, we need to check for the existence of `BTN_GAMEPAD` (`BTN_A/BTN_SOUTH`). Some gamepads don't entirely comply with the Linux Gamepad Specification, as they were made before it was written, to unify the handling of gamepads' button mappings. As such, some gamepads report their triggers as `ABS_Z` or `ABS_RZ`, for respectively the lower left trigger and the lower right trigger.

# Joysticks (flight-stick)

To detect a joytick, we need to check for the existence of `BTN_JOYSTICK` (`BTN_TRIGGER`, which every joystick has).
Joysticks have buttons on their baseplate, reported as `BTN_BASE`, `BTN_BASE[2-6]` and `BTN_PINKIE`. Those buttons don't have any defined use, so there only use is to be bound to whatever action the user want. The trigger button (behind the stick) is reported as `BTN_TRIGGER`. Tilting the stick is reported the same way as the left thumbstick of gamepads (`ABS_{X,Y}`, negative being left/front, positive being right/back). The upper (as in the nearest one to the front of the stick) thumb button, on the top of the stick, is reported with `BTN_THUMB`. The lower one is reported as `BTN_THUMB2`. There can be two extra buttons on the top of the stick, reported `BTN_TOP` and `BTN_TOP2` for the upper/lower button. Most joysticks can be twisted around the vertical axis to control the rudder of aircrafts. This axis is reported either as `ABS_RUDDER`, or `ABS_RZ`. Most joysticks also have a point-of-view hat (PoV hat), which is used to control the camera basically. It is reported as `ABS_HAT0X` for the left/right axis (`<0`/`>0`) and as `ABS_HAT0Y` for the up/down axis. Some joystick have integrated throttle control, reported as `ABS_THROTTLE`. But the throttle axis might be recorded as a completely separated input device, but it isn't any problem since there isn't any collision for this axis between different device types.

# Steering wheels

Steering wheels sometimes have gas and braking pedals, reported as `ABS_GAS` and `ABS_BRAKE`. The steering wheel axis is always reported as `ABS_WHEEL`. This axis is also reported by some pens, but we ignore it since we're only scanning for ID_INPUT_JOYSTICK devices, which excludes pens. When there's a gear box, the device will report `BTN_GEAR_{DOWN,UP}`. But it doesn't report the gear number, only gear changes events. As such, the neutral gear must be tracked on plugging (since logically you'd plug in with neutral gear), so that we can know if it's in reverse-mode (if we get a `BTN_GEAR_DOWN` when on neutral) and the gear number can be incremented on each `BTN_GEAR_UP` button event, and decremented on `BTN_GEAR_DOWN`.

Some wheels provide the same buttons as a regular gamepad, that is dpad, action-buttons, or buttons from joysticks. As all those aren't shared across device types for different functions (for example `ABS_RZ` is the joystick's twist while it is the gamepad's right lower trigger), we can take them in account in all cases.

# The rest

Every other axis or button reported by a game controller should be made available under some extra axis and extra buttons, that can safely be bound to extra functionnalities not provided by this device.