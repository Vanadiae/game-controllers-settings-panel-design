# Game controllers Settings panel

Game controllers that should be supported are joysticks and gamepads.

## Goals & scope

- Don't show axes and buttons in a generic way (i.e. by their internal numbering as reported by the kernel). As such, the position of the axes and buttons pressing state should be showed in the way it'll be handled by games, that is, as a gamepad (so a joystick will be mapped to a gamepad, as done by the SDL controllers' db that most games uses).
- Let test the force-feedback (rumble).
- Calibrate the different axes of the device
- Show up a notification when connecting a new game controller, to let the user calibrate and test the device straight away
- Provide some buttons and axes remapping functionality. See "The mappings problem" below.

## Non-Goals

- Support steering wheels. This is a complicated topic because there's a lot of different parameters that induce changes in the way the wheel acts. Also, there is the [Oversteer](https://github.com/berarma/oversteer) stand-alone app which seems to handle that already (not sure how good though), and it seems a bit more rare to encounter a steering wheel than other game controller anyway.
- Let test that the accelerometer or gyroscope works correctly. This one is quite tricky as it would probably require some 3D stuff, like a cube that rotates or something else. Testing those is also hard, and probably not very useful for much people.
- Provide a way to *manually* set the dead-zone and extreme axis values. The panel should already be providing good dead-zone values as part of the calibration process, and as such this wouldn't be necessary. This prevents from cluttering the UI for an expert-only feature.
- Show how the device really looks like in reality. This is nearly impossible and totally unscalable as it would require having one drawing for each game controller out there, for not much gain at the end.

## The mappings problem

Basically, the legacy Linux input drivers reports axis and buttons wrongly and not the same way depending on the game controller, but this can't be fixed for backward-compatible reasons… To fix this, a database recording the correct gamepad-layout mapping for the legacy linux input drivers mappings was created as part of the SDL library (which handles gamepads, with other things too). But this isn't enough, as not all game controllers are in the database, so you might need to do the mapping yourself (and eventually send it upstream so this one doesn't need manual mapping anymore). Also, some gamepads reports their identity (vendor and product ID) like an other legitimate gamepad, while they don't have the same buttons counts nor assignment (button A might then be reported as button X). This happens with both gamepad clones and legitimate third-party products. Those can't be sent upstream as they would conflict with some existing controller. This brings two problems. First, you can't play with both legitimate controller A and controller B that has same identity as A (i.e. multiplayer games), so the user must be informed of this, if some duplicated identity controllers were ever detected. But even if the person has duplicated identity controllers, he might still want to use one or another in single-player, so there must be some way of selecting which controller just got plugged-in so the correct mapping can be selected. Note that mappings only apply after restarting the game (or starting it after mapping the controller)

## Rational

- current stand-alone tools aren't very good (see relevant art) and don't really bring more than the available command-line tools like jstest and jscal (see relevant art for those two).
- they handle either only evdev or the legacy linux joystick API, but not both, which means axis calibration isn't working for all games
- they all provide a generic display, where axes and buttons are all identified by numbers, with axes position displayed as progress bars (see relevant art)
- Why not a separate app ? Because I think it's something that would be expected to be part of the default Settings app, and I find it quite cumbersome to have to install a separate application for this rather simple thing. Also, I don't think it deserves a separate app. Having it as part of Settings would mean you'd just plug in your game controller, then you get prompted straight away to calibrate your game controller and can fully profit from it. It's very handy.

## Calibration process

**Dead-zone:** the part at the middle of an axis (i.e. when the joystick is left untouched) where small movements shouldn't be recorded as an actual axis movement.

Calibrating an axis is the process of determining the dead-zone, the real maximum and minimum value (by opposition to the logical maximum and minimum, which is the full range of bytes that the device *can possibly report*).

Calibration is better done axis by axis, rather than all axis at once. Otherwise it's very frustrating as you have to make sure you center all axes without forgetting one of them. At least when doing one at a time, you can go step-by-step to calibrate each axis and so won't forget to center an axis.

## Relevant art
### KDE System Settings "Game controllers" panel

![kde](./relevant_art_screenshots/kde_system_settings_game_controllers_screenshot.png)

### jstest-gtk (standalone)

![dev_list](./relevant_art_screenshots/jstest_gtk_joysticks_list_screenshot.png)
![dev_axes_and_buttons](./relevant_art_screenshots/jstest_gtk_properties_dialog_screenshot.png)
![manual_calib](./relevant_art_screenshots/jstest_gtk_manual_calibration_screenshot.png)
![calib_wizard](./relevant_art_screenshots/jstest_gtk_calibration_wizard_screenshot.png)
![mappings](./relevant_art_screenshots/jstest_gtk_mappings_screenshot.png)

### evtest-qt (standalone)

![evtest-qt](./relevant_art_screenshots/evtest_qt_screenshot.png)

### Piper (tool for configuring leds and mouse buttons macros/mapping)

![piper](./relevant_art_screenshots/piper-screenshot.png)

### Windows 10

See https://www.maketecheasier.com/calibrate-game-controller-windows10/ for some images.

### SDL2 Gamepad Tool

![gamepad_tool](./relevant_art_screenshots/sdl_gamepad_tool.png)

### Games game controllers dialog

![gnome-games](./relevant_art_screenshots/Games-gamepad-screenshot.png)

### jstest

![jstest](./relevant_art_screenshots/jstest.png)

### jscal

![jscal](./relevant_art_screenshots/jscal.png)

## Tentative design

